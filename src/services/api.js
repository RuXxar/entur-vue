
const transformStopData = function (stopData) {
  const stopPlaces = stopData.data.stopPlaces;
  const estimatedCalls = stopPlaces[0].estimatedCalls;


  console.log(estimatedCalls);

  const journeys = estimatedCalls.map((estimatedCall) => {

    const line = estimatedCall.serviceJourney.line;

    const currentTime = new Date();
    const expectedTime = new Date(estimatedCall.expectedDepartureTime);
    const aimedTime = new Date(estimatedCall.aimedDepartureTime);

    const time = expectedTime.getMinutes() - currentTime.getMinutes();
    let timeNow = time.toString() + ' min';

    const isDelayed = expectedTime.getMinutes() - aimedTime.getMinutes() > 0;
    const isCancelled = estimatedCall.cancellation;

    if (time <= 0) {
      timeNow = 'Nå';
    }

    if (isCancelled) {
      timeNow = 'Innstilt';
    }

    return {
      publicCode: line.publicCode,
      type: line.transportMode,
      text: line.name,
      prettyName: estimatedCall.destinationDisplay.frontText,
      time: timeNow,
      isDelayed,
      isCancelled,
      journeyId: estimatedCall.serviceJourney.id
    };
  });

  return journeys;
};

export const fetchStopData = async function () {
  // Example POST method implementation:
  const response = await fetch('https://api.entur.io/journey-planner/v2/graphql', {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json',
      'ET-Client-Name': 'Ambita - hackaton'
    },
    body: JSON.stringify(getRequestData()) // body data type must match "Content-Type" header
  });
  const jsonResponse =  await response.json(); // parses JSON response into native JavaScript objects
  const transformedData = transformStopData(jsonResponse);
  return transformedData;
};

const getRequestData = function () {
  const date = new Date().toISOString();

  const requestData = {
    "query": "query ($ids: [String]!, $start: DateTime!, $timeRange: Int!, $limit: Int!, $limitPerLine: Int, $omitNonBoarding: Boolean!, $whiteListedLines: [String!], $whiteListedAuthorities: [String!], $whiteListedModes: [Mode]) { stopPlaces (ids: $ids) { id estimatedCalls (startTime: $start, timeRange: $timeRange, numberOfDepartures: $limit, numberOfDeparturesPerLineAndDestinationDisplay: $limitPerLine, omitNonBoarding: $omitNonBoarding, whiteListed: {lines: $whiteListedLines, authorities: $whiteListedAuthorities}, whiteListedModes: $whiteListedModes) { date forBoarding requestStop forAlighting situations { situationNumber summary { language value } description { language value } detail { language value } validityPeriod { startTime endTime } reportType infoLinks { uri label } } destinationDisplay { frontText } notices { text } aimedDepartureTime expectedDepartureTime realtime cancellation quay { id name publicCode description } serviceJourney { id publicCode transportSubmode journeyPattern { line { notices { text } } notices { text } } notices { text } line { id name publicCode notices { text } bookingArrangements { bookingMethods bookingNote minimumBookingPeriod bookingContact { phone url } } flexibleLineType transportMode description } } } } }",
    "variables": {
      "ids": [
        "NSR:StopPlace:58404"
      ],
      "start": date,
      "omitNonBoarding": true,
      "timeRange": 72000,
      "limit": 200,
      "limitPerLine": 3,
      "whiteListedModes": [
        "bus",
        "tram",
        "rail",
        "metro"
      ]
    }
  };

  return requestData;
};
